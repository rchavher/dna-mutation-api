var express = require('express');
var router = require('./routes/index');
var http = require('http');

var app = express();
const port = process.env.PORT ? process.env.PORT : '3011';

app.use(express.json());
app.use('/', router);
app.set('port', port);

var server = http.createServer(app);
server.listen(port, () => console.log('Server starting on port ', port));

module.exports = app;