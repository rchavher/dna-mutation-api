const mutationBusiness = require('../business/mutation');
const mutationController = require('../controllers/dnaMutation')
const bd = require('../data/bd');
const request = require("supertest");
const app = require("../app");

test("It has mutation", () => {
    let res = mutationBusiness.hasMutation([
        "GTCTTTAGTC",
        "ACTATCTCTA",
        "CTCCCCAGGT",
        "ACCTACGAAC",
        "GATTATATTA",
        "CGAATATGTG"
    ]);

    expect(res).toBe(true);
});

test("Not has mutation", () => {
    let res = mutationBusiness.hasMutation([
        "TTGGAAACTA",
        "TTAGATGTGA",
        "ACAAATAAAC",
        "CGCCCGCTGG",
        "ATTCGAGGCC",
        "ATGTCATTGC"
    ]);

    expect(res).toBe(false);
});

test("Not sending an array", () => {
    let res = mutationBusiness.hasMutation('Im not an array :(');
    expect(res).toBe(false);
});


describe("GET /stats ", () => {
    test("It should respond with a 200", async () => {
        const response = await request(app).get("/stats");
        expect(response.body).toHaveProperty("count_mutations");
        expect(response.statusCode).toBe(200);
    });
});