var mysql = require('mysql');
const dbConfig = require('../config/dbConfig')
var connection = mysql.createConnection(dbConfig);

let insertDnaTest = function (req) {
    req.isMutation = req.isMutation == true ? 1 : 0;
    connection.query('CALL `spDnaTest_Create`(\'' + req.dna + '\', ' + req.isMutation + ')', function (error, results, fields) {
        if (error) throw error;
    });
}


let getResults = function (callback) {
    connection.query('CALL `spDnaTest_GetResults`()', function (error, results, fields) {
        if (error) throw error;
        callback(results[0][0]);
    });
}


module.exports = { insertDnaTest, getResults }