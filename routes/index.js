const express = require('express');
const router = express.Router();
const dnaController = require('../controllers/dnaMutation');

router.post('/mutation', dnaController.hasMutation);
router.get('/stats', dnaController.getResults);

module.exports = router;
