
/**
 * Method to validate a DNA mutation
 * @param {} dna 
 * @returns 
 */
function hasMutation(dna) {
    if (!Array.isArray(dna))
        return false


    for (var i = 0; i < dna.length; i++) {
        let element = dna[i];

        for (var j = 0; j < element.length; j++) {
            let value = element[j];

            // Validates horizontal mutation
            if (validateValues([value, element[j + 1], element[j + 2], element[j + 3]], true))
                return true;

            // Validates vertical mutation
            if (dna[i + 1] && dna[i + 2] && dna[i + 3]) {
                if (validateValues([value, dna[i + 1][j], dna[i + 2][j], dna[i + 3][j]], false))
                    return true;
            }
        }
    }
    return false;
}

/**
 * Method to validate values
 * @param {*} values 
 * @param {*} isHorizontal 
 * @returns 
 */
function validateValues(values, isHorizontal) {
    let isRepeated = values.every((x, i, array) => x == array[0]);
    if (isRepeated == true)
        console.log('Mutacion', isHorizontal ? 'horizontal' : 'vertical', values[0]);

    return isRepeated;
}


/**
 *  TEST
 */
// let lenght = 10;
// for (var i = 0; i < 150; i++) {
//     console.log(hasMutation([createDna(lenght), createDna(lenght),
//     createDna(lenght), createDna(lenght),
//     createDna(lenght), createDna(lenght)]));
// }

// function createDna(length) {
//     var result = '';
//     var characters = 'ATCG';
//     for (var i = 0; i < length; i++) {
//         result += characters.charAt(Math.floor(Math.random() * characters.length));
//     }
//     return result;
// }


module.exports = { hasMutation };