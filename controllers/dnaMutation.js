const { hasMutation } = require('../business/mutation');
const { getResults, insertDnaTest } = require('../data/bd');
var mysql = require('mysql');
const dbConfig = require('../config/dbConfig')
const apiKey = '9af4d8381781baccb0f915e554f8798d'

/**
 * Method to validate if the dna test has a mutation
 * @param {*} req 
 * @param {*} res 
 */
exports.hasMutation = function (req, res) {
    if (isAuthorized(req.headers)) {
        if (req.body.dna) {
            let result = hasMutation(req.body.dna);

            insertDnaTest({
                dna: req.body.dna.join(),
                isMutation: result
            });

            res.sendStatus(result == true ? 200 : 403);
        }
        else {
            res.sendStatus(400)
        }

    } else {
        res.sendStatus(401);
    }
};

/**
 * Method to get the tests' results
 * @param {*} req 
 * @param {*} res 
 */
exports.getResults = function (req, res) {
    if (isAuthorized(req.headers)) {
        getResults(function (result) {
            res.send(result)
        })
    }
    else {
        res.sendStatus(401);
    }
}

function isAuthorized(headers) {
    return (headers && headers['x-api-key'] && headers['x-api-key'] == apiKey)
}
