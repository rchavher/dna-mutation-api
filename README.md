# DNA Mutation API



## Getting started
Para poder ejecutar este proyecto en tu maquina local, se requiere lo siguiente:
- Tener conexion a Internet
- Instalar NodeJS en su version 14.18.1 (https://nodejs.org/en/download/) (Reiniciar el equipo al concluir la instalación)

Una vez reiniciado el equipo, se puede abrir el proyecto en Visual Studio Code o navegar a la carpeta del mismo mediante la consola. Al estar en la carpeta, se requiere ejecutar el comando

`npm start`

Al concluir la ejecución, la aplicación correra en el puerto 3011.


# ¿De qué esta compuesto el proyecto?

## API
Se realizó una pequeña API basada en NodeJS y Express. Para el desarrollo de las pruebas unitarias se hizo uso del paquete Jest. Esta aplicacion se encuentra montada en una instancia Azure.

Para poder consumir los dos métodos expuestos, es requerido agregar el siguiente header en la llamada:

`x-api-key: 9af4d8381781baccb0f915e554f8798d`

A continuación se muestran las dos funcionalidades de esta API:

### Deteccion de mutación
URL: http://dnamutation.azurewebsites.net/mutation
Tipo: POST
Ejemplo de Request Body: 
```
{
    "dna": [
        "TCTGTCAGAT",
        "TTACAGGTCG",
        "TTTGAACAGA",
        "TTCGGCACTG",
        "AAATCGGACC",
        "TAGACAATAA"
    ]
}
```
Donde al obtener una mutacion, se devolverá el status 200 (OK) y sino, se devolverá un 403 (Forbidden)
### Obtener resultados
URL: http://dnamutation.azurewebsites.net/stats
Tipo: GET
Ejemplo de respuesta: 
```
{
    "count_mutations": 14,
    "count_no_mutations": 3,
    "ratio": 0.82
}
```

## Base de datos
La base de datos se hizo en MySQL, se compone de una tabla llamada t_DnaTests donde se almacena:
- **DnaTestId**: Id para identificar cada prueba
- **DnaTestContent**: El ADN enviado para la prueba
- **DnaTestIsMutation**: El resultado que define si contiene una mutacion o no

A su vez, cuenta con dos stored procedures:
- spDnaTest_Create: Para insertar un nuevo registro en la tabla t_DnaTests
- spDnaTest_GetResults: Para obtener los conteos de los resultados de las pruebas.

Esta base de datos se montó en una instancia RDS en AWS para poder ser utilizada en cualquier ambiente. Los datos de conexión se encuentran alojados en el archivo dbConfig.js dentro del folder config.


